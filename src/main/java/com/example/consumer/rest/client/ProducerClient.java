package com.example.consumer.rest.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "producer")
public interface ProducerClient {

    @GetMapping("/validate/even")
    String checkEvenOdd(@RequestParam Integer num);
}
