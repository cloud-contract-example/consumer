package com.example.consumer;

import com.example.consumer.rest.client.ProducerClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

@SpringBootTest
@AutoConfigureStubRunner(
        stubsMode = StubRunnerProperties.StubsMode.REMOTE,
        repositoryRoot = "git://https://gitlab.com/cloud-contract-example/contract.git",
        ids = "com.example:producer:0.0.1-SNAPSHOT")
class ConsumerApplicationBaseTests {

    @Autowired
    private ProducerClient producerClient;

    @Test
    void producerTest() {
        String result = producerClient.checkEvenOdd(2);
        Assertions.assertEquals("Even", result);
    }

}
